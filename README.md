# Расширение `SafeAuth` для браузера chrome #

## Установка ##
`npm i -D`

## Запуск ##
### Версия для разработки: ###
`gulp chrome:dev`  
Следит за изменениями и обновляет расширение и текущую вкладку;  
После запуска команды открываем `chrome`;  
Переходим в `chrome://extensions/`;  
`Режим разработчика`;  
`Загрузить распакованное расширение` -> `path/to/repo/chrome`;


### Версия для сборки: ###
`gulp chrome:production`  
Генерирует \*.crx файл, который нельзя протестировать, т.к. chrome не позволяет установить собранное расширение не из [webstore](https://chrome.google.com/webstore/category/extensions) ¯\\_(ツ)_/¯  

Счастливые обладатели [WebStorm](https://www.jetbrains.com/webstorm/) могут использовать встроенное меню:  
![gulp](https://user-images.githubusercontent.com/18496223/36653837-9affbed0-1ad1-11e8-8f92-85e06174e60a.png)  
![gulp2](https://user-images.githubusercontent.com/18496223/36653864-dcf290ce-1ad1-11e8-90ad-d48c1c42af34.png)

Инфраструктура проекта создавался с заделом на мультибраузерность, поэтому для примера есть задача `firefox:manifest`, предназначенная для сборки фала манифеста firefox. Это просто пример, сама логика расширения писалась направленно под хром. Для реальной работы в (как минимум firefox) нужно в некоторых местах использовать `browser = browser || chrome`, написать обертки на некоторые ф-ции, различающиеся порядком параметров, нормально настроить манифест и т.п.;  

## Основные задачи ##
- автоматически определять расположение поле ввода пароля на ресурсах mail.ru и buzzfeed.com/signin (будет плюсом поддержка других ресурсов);

- отображать в всплывающем сообщении рядом с полем ввода сложность пароля (ориентируясь на его длину и наборы используемых символов, конкретные критерии определить самостоятельно);

- рядом с обнаруженным полем ввода пароля отобразить иконку для скрытия/раскрытия всплывающего сообщения;

- меню extension открывается по кнопке на toolbar’е и содержит: а) команду для показа всплывающего сообщения со сложностью пароля (рядом с полем ввода) б) about с информацией об авторе.

## Дополнительные задачи ##
- Сохранение пользовательских настроек;
- Добавление выделенного пользователем элемента (временное, если мы не можем однозначно идентифицировать элемент и постоянное, если у элемента уникальный `id`, `name` или `class`);
- Автоматический поиск (по умолчанию отключен);

## Баги ##
На данный момент имеются проблемы с расположением тултипа и кнопки `SA`:  
*https://pikabu.ru/*  
![pikabu](https://user-images.githubusercontent.com/18496223/36653360-17922a40-1ace-11e8-8378-6403b9623165.png)  
*http://joyreactor.cc/login*  
![reactor](https://user-images.githubusercontent.com/18496223/36653361-17c1c53e-1ace-11e8-98ae-d4b831d48b8a.png)  
*https://www.buzzfeed.com/signin*  
![buzzfeed](https://user-images.githubusercontent.com/18496223/36653363-1826551c-1ace-11e8-9506-2395d7ae2c9e.png)  
*https://github.com/*  
![github](https://user-images.githubusercontent.com/18496223/36653364-18551334-1ace-11e8-98fd-82ed20cc0d47.png)

И с перерисовкой поля для ввода пароля при изменении размеров окна, на некоторых сайтах:  
![pikabu](https://user-images.githubusercontent.com/18496223/36653746-f7b78ac8-1ad0-11e8-8096-a29f53952cb9.gif)

## Использованные плагины ##
[gulp-crx-pack](https://github.com/PavelVanecek/gulp-crx)  
[gulp-merge-json](https://github.com/joshswan/gulp-merge-json)  
[run-sequence](https://github.com/OverZealous/run-sequence)  
[gulp-rename](https://github.com/hparra/gulp-rename)  
[del](https://github.com/sindresorhus/del)  
[crx-hotreload](https://github.com/xpl/crx-hotreload)  
