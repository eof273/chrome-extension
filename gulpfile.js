//Пройтись бебелем? Как он вообще с расширениями работает (chrome, browser и т.п.)?
'use strict';

const gulp = require('gulp'),
  fs = require('fs'),
  crx = require('gulp-crx-pack'),
  mergeJSON = require('gulp-merge-json'),
  runSequence = require('run-sequence'),
  del = require('del'),
  packageInfo = require('./package.json'),
  rename = require('gulp-rename');

// gulp.task('default', () => {
// });
let dev = false;

gulp.task('chrome:dev', (cb) => {
  dev = true;
  runSequence('chrome:build', 'watch:chrome', cb);
});

gulp.task('chrome:production', (cb) => {
  dev = false;
  runSequence('chrome:clear', 'chrome:build', 'chrome:pack', cb);
});

gulp.task('chrome:pack', () => {
  return gulp.src('./chrome')
    .pipe(crx({
      privateKey: fs.readFileSync('./key.pem', 'utf8'),
      filename: `${require('./chrome/manifest.json').name}.crx`
    }))
    .pipe(gulp.dest('.'));
});

gulp.task('chrome:build', ['chrome:manifest', 'chrome:src', 'chrome:locales']);

gulp.task('chrome:manifest', () => {
  const files = ['./src/manifest/manifest.json', './src/manifest/manifest_chrome.json'];

  if (dev) {
    files.push('./src/manifest/manifest_dev.json');
  }

  return gulp.src(files)
    .pipe(mergeJSON({
      startObj: {version: packageInfo.version},
      fileName: 'manifest.json',
      jsonSpace: '  ',
      concatArrays: true
    }))
    .pipe(gulp.dest('./chrome'));
});

gulp.task('firefox:manifest', () => {
  return gulp.src(['./src/manifest/manifest.json', './src/manifest/manifest_firefox.json'])
    .pipe(mergeJSON({
      fileName: 'manifest.json',
      jsonSpace: '  '
    }))
    .pipe(gulp.dest('./firefox'));
});

gulp.task('chrome:src', () => {
  const path = ['./src/**/*.+(png|html|js|css)'];

  if (!dev) {
    path.push('!./src/dev/*.*');
  }

  return gulp.src(path).pipe(rename(file => {
    file.dirname = '.';
  })).pipe(gulp.dest('./chrome'));
});

gulp.task('chrome:locales', () => {
  return gulp.src('./src/lang/*.json').pipe(rename(file => {
    file.dirname = file.basename;
    file.basename = 'messages';
  })).pipe(gulp.dest('./chrome/_locales'));
});

gulp.task('watch:chrome', () => {
  gulp.watch('./src/**/*.*', ['chrome:build']);
});

gulp.task('chrome:clear', () => {
  return del(['./chrome/*', './*.crx']);
});
