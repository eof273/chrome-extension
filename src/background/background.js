chrome.runtime.onMessage.addListener((msg, sender, sendResp) => {
  if (sender.id === chrome.runtime.id && msg) {
    if (msg.type === 'element-added') {
      alert(chrome.i18n.getMessage(msg.result ? `element_added${msg.added ? '' : '_temp'}` : 'no_password_field'));
    }
  }
});

function getStorage() {
  return new Promise((resolve, reject) => {
    chrome.storage.sync.get({
      auto: false,
      elements: [{
        url: 'https://mail.ru/',
        id: 'mailbox:password'
      }, {
        url: 'https://www.buzzfeed.com/signin',
        class: 'js-user-password'
      }]
    }, resolve);
  });
}

function setStorage(data) {
  return new Promise((resolve, reject) => {
    chrome.storage.sync.set(data, resolve);
  });
}
