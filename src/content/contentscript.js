'use strict';

// chrome.storage.sync.clear();

//@TODO Перерисовать кнопку после изменения размера окна - проблемы на https://pikabu.ru/;
//@TODO Обернуть поле в span и добавить кнопку???;

/**
 * Min password length;
 * @type {Number}
 */
const passwordLength = 4;

/**
 * Min count of unique symbols in password;
 * @type {Number}
 */
const passwordUniqueCount = 4;

const tooltipId = 'safe-auth-btn-tooltip';
const buttonId = 'safe-auth-btn';

let passwordLevel = 0;
let storage;
let state = 'none';

window.onload = safeAuth;

function getStorage() {
  return new Promise((resolve, reject) => {
    chrome.storage.sync.get({
      auto: false,
      elements: [{
        url: 'https://mail.ru/',
        id: 'mailbox:password'
      }, {
        url: 'https://www.buzzfeed.com/signin',
        class: 'js-user-password'
      }]
    }, resolve);
  });
}

function safeAuth() {
  getStorage().then(data => {
    storage = data;

    const currentURL = document.URL.split('?')[0];
    const element = data.elements.find(el => el.url === currentURL);
    const passwordInput = getPasswordInput(element);

    chrome.runtime.onMessage.addListener((msg, sender, sendResp) => {
      switch (msg) {
        case 'safe-auth-open-tooltip':
          sendResp(openTooltip(getPasswordInputByBtn()));
          break;
        case 'safe-auth-add-element':
          const context = {
            oldValue: ''
          };
          highlightElement = highlightElement.bind(context);

          document.addEventListener('mouseover', highlightElement);
          document.addEventListener('mouseout', highlightElement);
          document.addEventListener('click', highlightElement);

          document.addEventListener('click', addCustomElement);
          break;
        case 'safe-auth-get-tab-state':
          sendResp(state);
          break;
      }
    });

    if (passwordInput && window.getComputedStyle(passwordInput).display !== 'none') {
      initBtn(passwordInput);
    } else {
      console.error('Sorry, dude, something went wrong;');
    }

    chrome.runtime.sendMessage({type: 'tab-loaded'});
  });
}

function initBtn(passwordInput) {
  let btn = createBtn(passwordInput);
  state = 'found';

  window.addEventListener('resize', event => {
    btn.remove();
    passwordInput.style.width = '';
    btn = createBtn(passwordInput);
  });
}

/**
 * Check password safety;
 * @param {string} password
 * @return {Number} Count of passed checks [0-6];
 */
function checkPassword(password) {
  return (password.length >= passwordLength)
    + (/[A-Z]/.test(password))
    + (/[a-z]/.test(password))
    + (/[0-9]/.test(password))
    + (/[-_+!?@#&$%]/.test(password))
    // Count of unique symbols;
    + (password.split('')
      .filter((el, i, arr) => arr.indexOf(el) === i)
      .length >= passwordUniqueCount);
}

/**
 * Get translate for password description;
 * @param {Number} level [0;6]
 * @return {String}
 */
function getPasswordDescription(level) {
  return chrome.i18n.getMessage(`password_level_${level}`);
}

function createBtn(passwordInput) {
  //@TODO Корректно расположить кнопку;

  let btn = document.createElement('button');
  btn.id = buttonId;
  btn.innerText = 'SA';//chrome.i18n.getMessage('on');
  btn.className = `safe-auth-btn safe-auth-btn-level-${passwordLevel}`;
  btn.setAttribute('type', 'button');

  passwordInput.style.display = 'inline-block';//???
  passwordInput.parentNode.insertBefore(btn, passwordInput.nextSibling);

  btn.style.height = `${passwordInput.offsetHeight}px`;

  passwordInput.style.width = `${passwordInput.offsetWidth - btn.offsetWidth - 1}px`;

  btn.addEventListener('click', event => {
    openTooltip(passwordInput);
  });

  passwordInput.addEventListener('input', event => {
    passwordLevel = checkPassword(passwordInput.value);
    btn.className = `safe-auth-btn safe-auth-btn-level-${passwordLevel}`;

    //If tooltip is open - change description;
    let tooltip = document.getElementById(tooltipId);
    if (tooltip) {
      tooltip.innerText = getPasswordDescription(passwordLevel);
    }
  });

  return btn;
}

/**
 * Open/close tooltip;
 * @param {Element} passwordInput - password input element;
 * @return {Boolean|null}
 *  <b><i>true</i></b> - tooltip is open;
 *  <b><i>false</i></b> - tooltip is closed;
 *  <b><i>null</i></b> - There is no passwordInput;
 */
function openTooltip(passwordInput) {
  //@TODO Корректно расположить тултип;

  if (!passwordInput) {
    return null;
  }

  let tooltip = document.getElementById(tooltipId);

  if (!tooltip) {
    tooltip = document.createElement('div');
    tooltip.id = tooltipId;
    tooltip.className = 'safe-auth-btn-tooltip';
    tooltip.innerText = getPasswordDescription(passwordLevel);

    /*
    const passwordInputPosition = passwordInput.getBoundingClientRect();
    if (passwordInputPosition.left > window.innerWidth / 2) {
      // tooltip.style.left = `${passwordInputPosition.left}px`;
    }
    */

    passwordInput.parentNode.insertBefore(tooltip, passwordInput.nextSibling);

    tooltip.addEventListener('click', event => {
      tooltip.remove();
    });

    return true;
  } else {
    tooltip.remove();

    return false;
  }
}

/**
 * Get password input element;
 * @param {Object} element - {url, id/class/name};
 * @return {Element|null}
 */
function getPasswordInput(element) {
  let passwordInput = null;
  if (element) {
    //Url in list;
    if (element.id) {
      passwordInput = document.getElementById(element.id);
    } else if (element.name) {
      passwordInput = document.getElementsByName(element.name)[0];
    } else if (element.class) {
      passwordInput = document.getElementsByClassName(element.class)[0];
    }
  } else if (storage.auto) {
    //Url not in list;
    const lastTry = {
      id: document.getElementById('password'),
      type: document.querySelectorAll('[type*=password]')
    };

    if (lastTry.id) {
      passwordInput = lastTry.id;
    } else if (lastTry.type && lastTry.type.length === 1) {
      passwordInput = lastTry.type[0];
    }
  }

  return passwordInput;
}

function addCustomElement(event) {
  //@TODO Проверяем неколько элеметтов вверх-вниз для добавления в хранилище;

  const msg = {
    type: 'element-added',
    result: false,
    added: false
  };

  let target = event.target;
  event.preventDefault();
  event.stopPropagation();

  if (target.getAttribute('type') === 'password') {
    msg.result = true;

    //Thanks http://joyreactor.cc/login for this;
    let byId = [];
    try {
      byId = document.querySelectorAll(`[id=${target.getAttribute('id').replace(/([\[\]:])/g, '\\$1') || '""'}]`);
    } catch (e) {
    }
    const byName = document.getElementsByName(target.name);
    const byClass = document.getElementsByClassName(target.className);

    let newElement = {
      url: document.URL.split('?')[0]
    };
    if (byId.length === 1) {
      newElement.id = byId[0].id;
    } else if (byName.length === 1) {
      newElement.name = byName[0].name;
    } else if (byClass.length === 1) {
      newElement.class = byClass[0].className;
    } else {
      newElement = null;
    }

    if (newElement) {
      msg.added = true;

      getStorage().then(data => {
        data.elements.push(newElement);
        chrome.storage.sync.set({
          elements: data.elements
        });
      });
    }

    initBtn(target);
  }

  chrome.runtime.sendMessage(msg);

  document.removeEventListener('mouseover', highlightElement);
  document.removeEventListener('mouseout', highlightElement);
  document.removeEventListener('click', highlightElement);

  document.removeEventListener('click', addCustomElement);
}

function getPasswordInputByBtn() {
  const btn = document.getElementById(buttonId);
  return btn ? btn.previousSibling : null;
}

function highlightElement(event) {
  switch (event.type) {
    case 'mouseover':
      this.oldValue = event.target.style.backgroundColor;
      event.target.style.backgroundColor = '#D75050';
      break;
    case 'mouseout':
    case 'click':
      event.target.style.backgroundColor = this.oldValue;
      break;
  }
}
