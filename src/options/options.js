'use strict';

const bp = chrome.extension.getBackgroundPage();
let storage = {};
const timeouts = [];

document.addEventListener('DOMContentLoaded', restoreOptions);

chrome.runtime.onMessage.addListener((msg, sender, sendResp) => {
  if (sender.id === chrome.runtime.id && msg) {
    if (msg.type === 'element-added' && msg.added) {
      restoreOptions();
    }
  }
});

function deleteOptions() {
  const options = document.getElementById('options-select').options;

  if (options.selectedIndex > -1) {
    const selectedElementUrl = options[options.selectedIndex].value;
    storage.elements = storage.elements.filter(element => element.url !== selectedElementUrl);

    bp.setStorage({elements: storage.elements}).then(() => {
      chrome.tabs.query({
        url: selectedElementUrl
      }, tabs => {
        options.remove(options.selectedIndex);

        if (tabs[0]) {
          chrome.tabs.reload(tabs[0].id);
        }

        showMessage();
      });
    });
  }
}

function setAuto(auto) {
  bp.setStorage({auto: auto}).then(() => {
    //Reload???
    showMessage();
  });
}

function restoreOptions() {
  const selectLabel = document.getElementById('options-select-label');
  selectLabel.innerText = chrome.i18n.getMessage('options_select_label');

  const checkboxLabel = document.getElementById('options-auto-label');
  checkboxLabel.innerText = chrome.i18n.getMessage('options_auto_label');

  const checkbox = document.getElementById('options-auto');
  checkbox.addEventListener('change', event => {
    setAuto(checkbox.checked);
  });

  const saveBtn = document.getElementById('save');
  saveBtn.innerText = chrome.i18n.getMessage('delete');
  saveBtn.addEventListener('click', deleteOptions);

  bp.getStorage().then(data => {
    storage = data;

    checkbox.checked = storage.auto;

    const select = document.getElementById('options-select');
    select.options.length = 0;

    for (let i = 0, l = storage.elements.length; i < l; i++) {
      const element = storage.elements[i];
      const option = document.createElement('option');
      option.innerText = element.url;
      select.appendChild(option);
    }
  });
}

function showMessage() {
  const status = document.getElementById('status');
  status.textContent = 'Options saved.';

  timeouts.forEach((timeout) => clearTimeout(timeout));
  timeouts.push(
    setTimeout(() => {
      status.textContent = '';
    }, 1500)
  );
}
