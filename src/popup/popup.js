'use strict';

const aboutURL = 'https://github.com/eof273';

document.addEventListener('DOMContentLoaded', function () {
  initElement('btn-action', 'loading');
  initElement('btn-about', 'about', 'click', about);

  chrome.tabs.query({active: true, currentWindow: true}, tabs => {
    if (tabs[0].status === 'complete') {
      updateActionButton(tabs[0].id);
    } else {
      chrome.tabs.onUpdated.addListener((tabId, info) => {
        if (info.status === 'complete') {
          updateActionButton(tabs[0].id);
        }
      });
    }
  });
});

function updateActionButton(tabId) {
  chrome.tabs.sendMessage(tabId, 'safe-auth-get-tab-state', resp => {
    switch (resp) {
      case 'none':
        initElement('btn-action', 'add_element', 'click', addElement);
        break;
      case 'found':
        initElement('btn-action', 'open_tooltip', 'click', openTooltip);
        break;
    }
  });
}

function about() {
  chrome.tabs.create({url: aboutURL});
}

function openTooltip() {
  chrome.tabs.query({active: true, currentWindow: true}, tabs => {
    chrome.tabs.sendMessage(tabs[0].id, 'safe-auth-open-tooltip', resp => {
      //If chrome.runtime.onMessage.addListener(( , , sendResp)) got undefined -> it will return null;
      if (resp === null) {
        alert(chrome.i18n.getMessage('no_password_field'));
      }
      window.close();
    });
  });
}

function addElement() {
  chrome.tabs.query({active: true, currentWindow: true}, tabs => {
    chrome.tabs.sendMessage(tabs[0].id, 'safe-auth-add-element');
  });
}

/**
 * Init html element;
 * @param {String} id
 * @param {String} title
 * @param {String} event
 * @param {Function} listener
 * @return {Element}
 */
function initElement(id, title, event = null, listener = null) {
  const element = document.getElementById(id);
  element.innerText = chrome.i18n.getMessage(title);
  if (event || listener) {
    element.addEventListener(event, listener);
  }

  return element;
}
